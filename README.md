* Discription
This project will utilize Sockets for communication between processes.  
Your task is to write a simple web browser. 


The browser will: 
1)	Accept a URL as a command line argument or typed into the GUI address box.
2)	Connect to the web server (default port is 80 unless specified in the URL).
3)	Send an HTTP GET request to the server for the requested page.
(HTTP GET url:80)
4)	Parse the returned HTML or display returned error message.
5)	For any images in the HTML, send additional HTTP GET requests for each one.
6)	Output a page consisting of the text found in the page and the images.


Parsing:
1)	Most tags are ignored.  
2)	The image tag (<img src=…) should be recognized and processed.
3)	Though there are software libraries that can parse HTML, do not use these.  Just use the normal language features to do parsing. 

Rendering:
       For a GUI browser:
1)	Display the page content vertically.
2)	Display images where they should appear within the page content.

For a non-graphical (command-line based) browser:
1)	Display the page content vertically.
2)	For images, display “Image:  filename” where filename is the name of the image file which has been retrieved and stored in the current directory. 

* Design
JAVA
SOCKET