import java.net.*;
import java.lang.InterruptedException;
import java.io.*;

class ImageService implements Runnable{
	
	private String imgWebAddress = "";
	int imgport = 0;
	String imgHost = "";
	String get_imgurl = "";
	String fileName = "";
	
	ImageService(String imageurl, int port)
	{
		imgWebAddress = imageurl;
		imgport = port;
	}
	private void parseUrl(String url)
	{
		String lowerCase_str = url.toLowerCase();
		if(lowerCase_str.startsWith("http://"))//uppercase?lowercase?
		{
			
			int host_start = new String("http://").length();
			String subWebAddress = "";
			if(lowerCase_str.startsWith("http://"))
			{
				subWebAddress = url.substring(host_start);
			}
			else
				subWebAddress = url;
				
			//System.out.println("subwebaddr is " + subWebAddress);
			int host_end = subWebAddress.indexOf('/');
			if(host_end == -1)
			{
				host_end = subWebAddress.length() -1;
			}
			int index_colon = subWebAddress.indexOf(':');
			if((-1 != index_colon) && (index_colon < host_end))
			{
				StringBuffer sb = new StringBuffer();
				int j = index_colon + 1;
				char ch = subWebAddress.charAt(j);
				while(('0' <= ch) && (ch <= '9'))
				{
					j++;
					ch = subWebAddress.charAt(j);
				}
				if(j > index_colon + 1)
				{
					String sport = subWebAddress.substring(index_colon + 1, j);
					imgport = Integer.valueOf(sport);
					//System.out.println("Port is got from web address, and it is " + imgport);
					host_end = index_colon; 
					//System.out.println("host_end is " + host_end);
					imgHost = subWebAddress.substring(0, host_end);
					//System.out.println("host is " + imgHost);
					get_imgurl = subWebAddress.substring(j);
					//System.out.println("get url is "+get_imgurl);
				}
			}
			else
			{
				//System.out.println("ImageService: host_end is " + host_end);
				imgHost = subWebAddress.substring(0, host_end);
				//System.out.println("ImageService: host is " + imgHost);
				get_imgurl = subWebAddress.substring(host_end);
				//System.out.println("ImageService: get url is "+get_imgurl);
			}
			
			//get filename
			int ind = subWebAddress.lastIndexOf('/');
			fileName = subWebAddress.substring(ind + 1);
		}
	}

	public void run()
	{
		parseUrl(imgWebAddress);
		try{
			Socket imgSocket = new Socket(imgHost, imgport);
			//System.out.println("ImageService: thread run");
			BufferedWriter img_out = new BufferedWriter(new OutputStreamWriter(imgSocket.getOutputStream()));
			InputStream soc_in  = imgSocket.getInputStream();
			
			StringBuffer sb = new StringBuffer();
			
			sb.append(WebBrowser.GET + get_imgurl+ WebBrowser.HTTP_VERSION + "\r\n")
			.append("Host: "+ imgHost + "\r\n")
			.append("Connection: close\r\n")
			.append("\r\n");
			//System.out.println("ImageService: "+ sb.toString());
			img_out.write(sb.toString());
			img_out.flush();
			
			
			FileOutputStream fos;
			if(fileName.length() == 0)
				fos = new FileOutputStream("image1.jpg");
			else
				fos = new FileOutputStream(fileName);
			int file_size_counter = 0;
			int file_size = -1;
			int len = 0;
			int tmpIndex = -1;
			boolean image_start = false;
			byte buffer[] = new byte[2048];
			boolean statusOK = false;
			while((len = soc_in.read(buffer, 0, 2048)) != -1)
			{
				if(file_size == -1)
				{
					String str_stream = new String(buffer);
					//System.out.println(str_stream);
					if(statusOK == false)
					{
						if(str_stream.toLowerCase().indexOf(new String("200 ok")) == -1)
						{
							//System.out.println("response code is not 200 OK");
							fos.close();
							File f = null;
							if(fileName.length() == 0)
							{
								f = new File(fileName);
								
							}
							else
							{
								f = new File(fileName);
							}
							if(f.exists())
								f.delete();
							
							break;
							
						}
						else
						{
							//System.out.println("response code is 200 OK");
							statusOK = true;
						}
					}
					
					tmpIndex = str_stream.indexOf("Content-Length");
					if(tmpIndex != -1)
					{
						tmpIndex += new String("Content-Length:").length();
						char ch = '\0';
						boolean begin = false;
						StringBuffer sbsize = new StringBuffer();
						while(tmpIndex < str_stream.length())
						{
							ch = str_stream.charAt(tmpIndex);
							if('0' <= ch && ch <= '9')
							{
								begin = true;
								sbsize.append(ch);
							}
							else
							{
								if(begin == true)
									break;
							}
							tmpIndex ++;
						}
						file_size = Integer.valueOf(sbsize.toString());
						//System.out.println("Image size from HTTP header = " + file_size);
					}
				}
				//System.out.println("len = " + len);
				if(image_start == false)
				{
					boolean finder = false;
					byte[] ref = {13,10,13,10};
					int i = 0;
					int j = 0;
					while(i < len && j <4)
					{
						if(finder == false && buffer[i] == ref[0])
						{
							finder = true;
							j ++;
						}
						else if(buffer[i] != ref[j])
						{
							finder = false;
							j = 0;
						}
						else if(buffer[i] == ref[j])
						{
							j ++;
						}
						i++;
					}
					if(finder == true)
					{
						//System.out.println("image start offset = " + i);
						image_start = true;
						fos.write(buffer, i, len - i);
						file_size_counter += len - i;
					}
					
				}
				else
				{
					fos.write(buffer, 0, len);
					file_size_counter += len;
				}
				if(file_size_counter >= file_size && file_size != -1)
					break;
			}
			if(statusOK == true)
			{
				//System.out.println(" file_size_counter= " + file_size_counter);
				//System.out.println("Image saved!");
				fos.close();
			}
			soc_in.close();
			img_out.close();
			imgSocket.close();
			
		}catch(UnknownHostException e)
		{
			System.out.println("Unknown image host: " + imgHost);
		}catch(IOException e)
		{
			System.out.println("IOException in imageService: " + e.getMessage());
		}
	}
}
public class WebBrowser {
	static final String GET = "GET ";
	static final String HTTP_VERSION = " HTTP/1.1";
	static final String HTTP_TAG_A = "a href";
	private static final String HTTP_TAG_IMAGE = "img";

	private String host = null;
	private int port = 0;
	private String host_dir = null;
	//static String exp_host = "http://www.december.com/html/demo/hello.html";
	//static String exp_host = "http://www.utdallas.edu/~ozbirn/image.html";
	//static String exp_host = "http://assets.climatecentral.org/images/uploads/news/Earth.jpg";
	//static String exp_host = "http://htmldog.com/examples/images1.html";
	//static String exp_host = "http://htmldog.com/examples/images/sifaka.jpg";
	//static String exp_host = "http://portquiz.net:8080/";
	//static String exp_host = "http://www.utdallas.edu/os.html";
	private String get_url = null;
	private static final int def_port = 80;
	private Socket mySocket = null;
	boolean portinaddr = false;
	private Thread t = null;
	private boolean checkImage(String url)
	{
		
		if(url.toLowerCase().endsWith(".jpg"))
			return true;
		else if(url.toLowerCase().endsWith(".gif"))
			return true;
		else if(url.toLowerCase().endsWith(".png"))
			return true;
		else if (url.toLowerCase().endsWith(".jpeg"))
			return true;
		else 
			return false;
	}
	private int parseWebAddress(String webAddress)
	{
		
		if(webAddress.startsWith("http://"))//uppercase?lowercase?
		{
			if(true == checkImage(webAddress))
			{
				System.out.println("Image: " + webAddress.substring((webAddress.lastIndexOf('/') +1)));
				ImageService imgSer = new ImageService(webAddress, port);
				t = new Thread(imgSer);
				t.start();
				
				return 1;
			}
			int host_start = new String("http://").length();
			String subWebAddress = "";
			if(webAddress.startsWith("http://"))
			{
				subWebAddress = webAddress.substring(host_start);
			}
			else
				subWebAddress = webAddress;
				
			//System.out.println("subwebaddr is " + subWebAddress);
			
			int host_end = subWebAddress.indexOf('/');
			if(host_end == -1)
			{
				host_end = subWebAddress.length() -1;
			}
			int index_colon = subWebAddress.indexOf(':');
			if((-1 != index_colon) && (index_colon < host_end))
			{
				int j = index_colon + 1;
				char ch = subWebAddress.charAt(j);
				while(('0' <= ch) && (ch <= '9'))
				{
					j++;
					ch = subWebAddress.charAt(j);
				}
				if(j > index_colon + 1)
				{
					String sport = subWebAddress.substring(index_colon + 1, j);
					port = Integer.valueOf(sport);
					host_end = index_colon; 
					host = subWebAddress.substring(0, host_end);
					get_url = subWebAddress.substring(j);
					host_dir = webAddress.substring(0, webAddress.lastIndexOf("/"));
					//System.out.println("host_dir " + host_dir);
				}
				return 0;
			}
			host = subWebAddress.substring(0, host_end);
			//System.out.println("host is " + host);
			get_url = subWebAddress.substring(host_end);
			host_dir = webAddress.substring(0, webAddress.lastIndexOf("/"));
			//System.out.println("host_dir " + host_dir);
			
		}
		return 0;
	}
		
	private String RatifyURL(String url)
	{
		String newUrl = null;
		if(!url.toLowerCase().startsWith("http://"))
		//if(!url.contains(".edu") && !url.contains(".org") && !url.contains(".net") && !url.contains(".com") && !url.contains(".cn")) //no host
		{
			if(!url.startsWith("/"))
			{
				newUrl = new String(host_dir+ "/" + url);
			}
			else
			{
				newUrl = new String(host_dir + url);
			}
			return newUrl;
		}
		return url;
	}
	private void parseHtml(String content)
	{
		int i = 0;
		int tmpval = 0;
		char ch = '\0';
		
		String subString = "";
		String low_subString = "";
		String subContent = "";
		String low_subContent = "";
		
		String img_url = "";
		
		boolean tag_start = false;
		boolean enter_insert = false;
		boolean nesttag = false;
		
		if(content.contains("HTTP/1.1 404 Not Found"))
		{
			System.out.println("404, Page not found.");
			return;
		}
		//System.out.println("parseHtml start");
		tmpval = content.indexOf("<html"); //"<html xmlns="http://www.w3.org/1999/xhtml">

		subContent = content.substring(tmpval + new String("<html").length());
		low_subContent = subContent.toLowerCase();
		
		while (i < subContent.length())  // not the end
		{
			ch = subContent.charAt(i);
		
			if(ch == '>')
			{
				i++;
				continue;
			}
			else if(ch == '<')
			{
				if(subContent.charAt(i+1) == '/')
				{
					subString = subContent.substring(i);
					low_subString = low_subContent.substring(i);
					if(low_subString.startsWith("</p>"))
					{
						if( enter_insert == false)
						{
							System.out.println("\r\n");
							enter_insert = true;
						}

					}
					else if(low_subString.startsWith("</title>"))
					{
						if( enter_insert == false)
						{
							System.out.println("\r\n");
							enter_insert = true;
						}

					}
					else if(low_subString.startsWith("</h1>"))
					{
						if( enter_insert == false)
						{
							System.out.println("\r\n");
							enter_insert = true;
						}

					}
					else if(low_subString.startsWith("</h2>"))
					{
						if( enter_insert == false)
						{
							System.out.println("\r\n");
							enter_insert = true;
						}

					}
					if(nesttag == false)
					{
						tag_start = false;
						if( enter_insert == false)
						{
							System.out.println("\r\n");
							enter_insert = true;
						}

					}
					else
						nesttag = false;
					
				}
				else if(low_subContent.substring(i+1).startsWith(HTTP_TAG_IMAGE))  //img
				{
					String image = subContent.substring(i);
					int src_start = image.indexOf("src=");
					
					if(src_start != -1)
					{
						src_start += new String("src=").length() +1;
						int src_end = src_start;
						while((image.charAt(src_end) != '\"') &&(image.charAt(src_end) != '\''))
						{
							src_end ++;
						}
						String fileName = null;
						img_url = image.substring(src_start, src_end);
						//System.out.println("image url: " + img_url);
						img_url = RatifyURL(img_url);
						fileName = img_url.substring((img_url.lastIndexOf('/') + 1));
						System.out.println("Image: " + fileName);
						//System.out.println("after ratified, image url: " + img_url);
						ImageService imgSer = new ImageService(img_url, port);
						t = new Thread(imgSer);
						t.start();
						
					}
				
				}
				else if(low_subContent.substring(i).startsWith("<br>"))  //break
				{
					if( enter_insert == false)
					{
						System.out.println("\r\n");
						enter_insert = true;
					}
				}
				else if(low_subContent.substring(i).startsWith("<p>"))  //paragraph
				{
					if( enter_insert == false)
					{
						System.out.println("\r\n");
						enter_insert = true;
					}
				}
				else if(low_subContent.substring(i).startsWith("<style"))
				{
					low_subString = low_subContent.substring(i);
					int n = low_subString.indexOf("</style>");
					if(n != -1)
					{
						i = i+n + new String("</style>").length();
					}
					continue;
				}
				else if(low_subContent.substring(i).startsWith("<script"))
				{
					low_subString = low_subContent.substring(i);
					int n = low_subString.indexOf("</script>");
					i += n + new String("</script>").length();
					tag_start = false;
					continue;
				}
				else if(low_subContent.substring(i).startsWith("<!--"))
				{
					low_subString = low_subContent.substring(i);
					int n = low_subString.indexOf("-->");
					i += n + new String("-->").length();
					tag_start = false;
					continue;
				}

				else if(tag_start == true)
					nesttag = true;
				else
					tag_start = true;
				
				subString = subContent.substring(i);
				tmpval = subString.indexOf('>');
				if(tmpval != -1)
				{
					i += tmpval+1;
				}
				else
					i +=1;
				
				continue;
			}
			else
			{
				//&copy?//&nbsp?//amp
				subString = subContent.substring(i);
				if(ch == '&')
				{
					if(subString.startsWith("&nbsp;"))
					{
						ch = '\240';  //oct
						i = i + new String("&nbsp;").length();
					}
					if(subString.startsWith("&copy;"))
					{
						ch = '\251';
						i = i + new String("&copy;").length();
					}
					else if(subString.startsWith("&amp;"))
					{
						ch = '\46';
						i = i + new String("&amp;").length();
					}
					enter_insert = false;
					System.out.print(ch);
					continue;
				}
				enter_insert = false;
				System.out.print(ch);
			}
			
			i++;
		}

	}
	int ListenSocket(String webAddress, int input_port) throws UnknownHostException, IOException{
		port = input_port;
		if(0 == parseWebAddress(webAddress))
			mySocket = new Socket(host, port);	
		else 
			return 1;
		
		return 0;
	}
	void Communicate() throws UnknownHostException, IOException {
		
		BufferedWriter out = new BufferedWriter(new OutputStreamWriter(mySocket.getOutputStream()));
		BufferedReader in = new BufferedReader(new InputStreamReader(mySocket.getInputStream()));
		
		StringBuffer sb = new StringBuffer();
		
		sb.append(GET + get_url+ HTTP_VERSION + "\r\n")
		.append("Host: "+ host + "\r\n")
		.append("Connection: close\r\n") 
		.append("\r\n");
		//System.out.println(sb.toString());
		out.write(sb.toString());
		out.flush();
		String line = "";
		StringBuffer pageContent = new StringBuffer();
		
		while((line = in.readLine()) != null)
		{
			pageContent.append(line);
			//System.out.println(line);
		}
		mySocket.close();
		parseHtml(pageContent.toString());
		
		return;			
	}
	static private boolean portValidity(String sPort)
	{
		int i = 0;
		while(i<sPort.length())
		{
			if(sPort.charAt(i)>'9' || sPort.charAt(i) < '0')
				return false;
			i++;
		}
		return true;
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int n = args.length;
		String webAddr = null;
		int port = 0;
		switch(n)
		{
		case 0:
			System.out.println("Please enter as: programname website [port]");
			return;
		case 1:
			webAddr = args[0];
			port = def_port;
			break;
		case 2:
		default:
			webAddr = args[0];
			if(true == portValidity(args[1]))
				port = Integer.parseInt(args[1]);
			else
			{
				System.out.println("Port is invalid, use 80 instead!");
				port = def_port;
			}
			break;
		}
		
		//System.out.println("webAddress is: " + webAddr + "port is " + port);
		
		WebBrowser myWebBrowser = new WebBrowser();
		try
		{
			if(0 == myWebBrowser.ListenSocket(webAddr, port))
			//if(0 == myWebBrowser.ListenSocket(exp_host, def_port))
				myWebBrowser.Communicate();

		}catch(UnknownHostException e){
			System.out.println("Unknown host");
			
		}catch(IOException e){
			System.out.println("IOException: " + e.getMessage());
			
		}finally{
			
			
		}

	}

}
